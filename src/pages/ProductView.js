import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {

	// The "useParams" hook allows us to retrieve the courseId passed via the URL
	const { productId } = useParams();

	//to be able to obtain the user ID so that we can enroll a user
	const {user} = useContext(UserContext)

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);

	useEffect(() => {
		console.log(productId);

		// Fetch request that will retrieve the details of the course from the database to be displayed in the "CourseView" page
		fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	//Function to enroll a user
	const addToCart = (productId, quantity) =>{

		fetch(`${process.env.REACT_APP_API_URL}/users/cart`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true) {
				Swal.fire({
					title: "Successfully enrolled",
					icon: "success",
					text: "You have successfull enrolled to this course"
				})
			}else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3}}>
					<Card>
						<Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text>
					        <Card.Subtitle>Class Schedule</Card.Subtitle>
					        <Card.Text>8 am - 5 pm</Card.Text>

					        {user.id !== null
					        	?
					        	<Button variant="primary" onClick={() => addToCart(productId)}>Enroll</Button>
					        	:
					        	<Button as={Link} to="/login" variant="danger">Log in to enroll</Button>
					    	}
					    </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}