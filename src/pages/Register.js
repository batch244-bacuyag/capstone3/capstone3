import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

	const {user} = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// Check if values are successfullt binded
	console.log(firstName);
	console.log(lastName);
	console.log(mobileNumber);
	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(() => {
		const nameRegex = /^[a-zA-Z ]*$/;
		const mobileRegex = /^\d{11,}$/;

		if (email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '' && mobileNumber !== '' && nameRegex.test(firstName) && nameRegex.test(lastName) && mobileRegex.test(mobileNumber) && password1 === password2) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password1, password2, firstName, lastName, mobileNumber]);

	// Function to simulate user registration
	function registerUser(e) {

		// Prevents the page reloading
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Duplicate email found!",
					icon: "error",
					text: "Kindly provide another email to complete the registration."
				})

			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNumber,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data === false){
						Swal.fire({
							title: "Registration Successful!",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						// Clear input fields
						setFirstName('');
						setLastName('');
						setMobileNumber('');
						setEmail('');
						setPassword1('');
						setPassword2('');

					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		})	

		// alert('Thank you for registering!');
	}

	// Validation to enable submit button when all input fields are populated and both passwords match


	return	(
		(user.id !== null)
		?
			<Navigate to="/products"/>
		:

		// Invokes the registerUser function upon clicking on submit button

		<Form onSubmit={(e) => registerUser(e)}>

			<Form.Group controlId="userFirstName">
			    <Form.Label>First Name</Form.Label>
			    <Form.Control
			        type="text"
			        placeholder="Enter First Name"
			        required
			        value={firstName}
			        onChange={e => setFirstName(e.target.value)}
			    />
			</Form.Group>

			<Form.Group controlId="userLastName">
			    <Form.Label>Last Name</Form.Label>
			    <Form.Control
			        type="text"
			        placeholder="Enter Last Name"
			        required
			        value={lastName}
			        onChange={e => setLastName(e.target.value)}
			    />
			</Form.Group>

			<Form.Group controlId="userMobileNumber">
			    <Form.Label>Mobile Number</Form.Label>
			    <Form.Control
			        type="tel"
			        placeholder="Enter Mobile Number"
			        required
			        value={mobileNumber}
			        onChange={e => setMobileNumber(e.target.value)}
			    />
			    <Form.Text className="text-muted">Please enter a mobile number with at least 11 digits.</Form.Text>			
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>			
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
				/>			
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}
				/>		
			</Form.Group>

			{/*conditional render submit button based on isActive state*/}

			{isActive	
				?
					<Button variant	="primary" type="submit" id="submitBtn">Submit</Button>
				:
					<Button variant	="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			}
			
		</Form>
	)
}