import {Container} from 'react-bootstrap';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';



export default function Home() {

	const data = {
		title : "Welcome to Sneakpick",
		content : "where kicks are always served fresh",
		destination: "/products",
		label: "Shop now!"
	}
	return (
		<>
			<Container>
{/*              	<Banner data={data}/>*/}
              	<br/>
              	<Highlights/>
            </Container>
		</>
	)
}