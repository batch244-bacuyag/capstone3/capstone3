import { useContext } from 'react'
import {  Navbar, Container, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import pic from "../sneakpick.png"
import './AppNavbar.css'
import SearchIcon from '@mui/icons-material/Search';
import ShoppingCartRoundedIcon from '@mui/icons-material/ShoppingCartRounded';
import NavDropdown from 'react-bootstrap/NavDropdown';

export default function AppNavbar(){

  const { user } = useContext(UserContext);

  // State to store the user information stored in the login page
  // const [user, setUser] = useState(localStorage.getItem('email'));
  // console.log(user);

	return(
      <Navbar bg="white" expand="lg">
        <Container>
          <Navbar.Brand as={Link} to="/" show>
            <div style= {{display: 'flex', alignItems: 'center', flexwrap: 'wrap'}}>
              <img className = "navbar_logo"src ={pic}/>
            </div>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" className='ml-auto'/>
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto flex-grow-1">
              <div className="navbar_search d-flex align-items-center w-100">
                  <input className="navbar_searchInput" style={{ flexGrow: 1 }} />
                  <SearchIcon className="navbar_searchIcon pt-1" style={{marginLeft: '-25px', marginRight: '5px'}} />
              </div>
              <Nav.Link as={NavLink} to="/products">Kicks</Nav.Link>

              {/*Conditional rendering such that the Logout link will be shown instead of the Login and Register when a user is logged in*/}

                <>
                <Nav>
                  <NavDropdown
                    id="nav-dropdown-dark-example"
                    title="Access"
                    menuVariant="light"
                  >
                {(user.id !== null)

                  ?
                  <>
                  <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.2">Account Information</NavDropdown.Item> 
                  <NavDropdown.Item href="#action/3.2">My Purchases</NavDropdown.Item> 
                  <NavDropdown.Item href="#action/3.2">Reset Password</NavDropdown.Item> 
                  </>                 
                  :
                  <>
                  <NavDropdown.Item href="/login">Login</NavDropdown.Item>
                  <NavDropdown.Item href="/register">
                      Don't have an account? Register
                  </NavDropdown.Item>
                  </>
                  }
                  </NavDropdown>
                </Nav>                
                </>
              
              <Nav.Link as={NavLink} to="/products">
                  <ShoppingCartRoundedIcon className="navbar_shoppingCartIcon"/>
              </Nav.Link>

            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
	)
}

//export default AppNavbar (can also be used)