import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Banner.css';

export default function Banner({data}) {

	const { title, content, destination, label } = data;

	return (
		<Row id="bk-color">
			<Col className="p-5 text-center" >
				<h1>{title}</h1>
				<p>{content}</p>
				<Button as={Link} to={destination} variant="primary">{label}</Button>
			</Col>
		</Row>
	)
}