// import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';

export default function ProductCard({productProp}) {

	// Decontruct the course properties into their own variables
	const { _id, name, description, price} = productProp;

	// Syntax:
	// const [getter, setter] = useState(initialGetterValue)
	// const [ count, setCount ] = useState(0);

	// useState hook for getting and setting the seats for the course
	// const [ seats, setSeats ] = useState(30);

	// const [ isOpen, setIsOpen ] = useState(false);


	// Function that keeps track of the enrollees for a course
	// By default JavaScript is synchronous it executes code from the top of the file all the way to the bottom and will wait for the completion of one expression before it proceeds to the next
    // The setter function for useStates are asynchronous allowing it to execute separately from other codes in the program
    // The "setCount" function is being executed while the "console.log" is already completed resulting in the value to be displayed in the console to be behind by one count
	// function enroll(){
	// 	// if (seats > 0) {
	// 		setCount(count + 1);
	// 		console.log('Enrollees: ' + count);
	// 		setSeats(seats - 1);
	// 		console.log('Seats: ' + seats);
		// } else {
		// 	alert('No more seats available');
		// }
		
	// }

	// Define a "useEffect" hook to have the "CourseCard" component do perform a certain task after every DOM update
    // This is run automatically both after initial render and for every DOM update
   	// React will re-run this effect ONLY if any of the values contained in the seats array has changed from the last render / update

	// useEffect(() => {
	// 	if(seats === 0) {
	// 		setIsOpen(true);
	// 	}
	// }, [seats]);

    return (
	    <Card>
	        <Card.Body>
	        	
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>Php {price}</Card.Text>
	            <Button as={Link} variant="primary" to={`/products/${_id}`}>Details</Button>
	        </Card.Body>
	    </Card>  

    )
}